import React from "react";
import styled from "styled-components";

const Card = styled.div`
display: flex;
justify-content: center;
width: 90px;
height: 90px;
border-radius: 6px;
background-image: url(${(props) => props.photo});
background-color: blue;
background-size: cover;
`;

const Title = styled.span`
color: #ffffff;
text-align: top;
padding: 20px; 
font-size: 20px
`;

const ImageCard = ({ photo, title }) => ( 
    <Card photo={photo}>
        <Title>{title}</Title>
    </Card>
);

export default ImageCard